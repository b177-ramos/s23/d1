/*Setting up MongoDB Atlas
1. Start by creating an account in MongoDB Atlas using your Google Account.
2. Upon registering an account on MongoDB Atlas, you will now create a cluster:
a. Cloud Provider and Region: AWS & Singapore
b. Change Cluster Name: WDC028-Course-Booking
c. Wait for the cluster to be created.
3. After the cluster is created, click the connect button to configure the following:
a. For the Add a connection IP:
 - Click the Allow Access from Anywhere option. 
- It will show a field with IP address of 0.0.0.0/0. 
- Click the Add IP Address button.
b. For the Create a Database User:
-  add a user with both username and password of admin*/


// select a database
// -- when creating a new database via a command line, the use command can be enetered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created.

// use databaseName

// Different level of data
// database = filing cabinet
// - collection = drawer
// - document/record = folder inside a drawer
// - sub-documents (optional) = other files
// - fields = file/folder content

/* e.g. document with no sub-documents:
	{
		name: "Juju Unti",
		age: 33,
		occupation: "Instructor"
	}
*//*
	e.g. document with sub-documents:
	{
		name: "Juju Unti",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 street", 
			city: "Makati", 
			country: "Philippines"
		}
	}
*/

// Embedded vs Referenced Data

//Insert One Document (Create)

// db.users.insert({
// 	firstName: "Jane",
// 	lastname: "Doe"
// 	age: 21,
// 	contact: {
// 		phone: "123456789",
// 		email: "janedoe@mail.com"
// 	},
// 	courses: ["CSS", "JavaScript", "Python"],
// 	department: "none"
// })

//if inserting/creating a new document within a collection that does not exist, MongoDB will automatically create that collection


//Insert Many Document

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])




//Finding documents (Read)
//retrieve a list of all users:
db.users.find()

//find a specific user:
db.users.find(		//finds any and all matches
	{firstName: "Stephen"}
)

db.users.findOne(		//finds only the first match
	{firstName: "Stephen"}
)

//Finding documents with multiple parameters/conditions (Read)
db.users.find(
	{lastName: "Armstrong", age: 82}
)



//Update/Edit Documents
//create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "1234567890",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
})

//updateOne always need to be provided two objects. The first object specifies which document to update. The second object contains the $set operator, and inside the $set operator are the specific fields to be updated.
//updates one document
db.users.updateOne(
	{_id: ObjectId("62876c75328bf6df207743ea")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	}
)
//updates multiple documents
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)

//Deleting a single document
//creating a document to deleete
db.users.insert({
	firstname: "Test"
})
//deleting a single document
db.users.deleteOne({
	firstname: "Test"
})

//Deleting many documents
//Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection
//creating a document to delete
db.users.insert({
	firstName: "Bill"
})

//Deleting many
db.users.deleteMany({
	firstName: "Bill"
})
